# Demo Faster R-CNN


# How to run this project by command lines

## Detect an Image
python detect.py --input input/people.jpg

python detect.py --input input/people.jpg --min-size 1024

python detect.py --input input/street.jpg

python detect.py --input input/street.jpg --min-size 1024

## Detect a Video
python detect_vid.py --input input/video2.mp4

python detect_vid.py --input input/video2.mp4 --min-size 300

# Reference
https://debuggercafe.com/faster-rcnn-object-detection-with-pytorch/